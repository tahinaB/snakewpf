﻿using System.Windows;

namespace Snake
{
    internal class SnakeHighscore(string pPlayerName,int pScore)
    {
        public string PlayerName
        {
            get => pPlayerName;
            set
            {
                if (string.Equals(pPlayerName,value))
                {
                    return;
                }
                pPlayerName = value;
            }
        }

        public int Score
        {
            get => pScore;
            set
            {
                if (pScore == value)
                {
                    return;
                }
                pScore = value;
            }
        }

        public SnakeHighscore():this(string.Empty,0)
        {
            
        }
    }
}
