﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace Snake
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Fields
        const int SnakeSquareSize = 20;
        const int SnakeStartLength = 3;
        const int SnakeStartSpeed = 400;
        const int SnakeSpeedThreshold = 100;
        const int MaxHighscoreListEntryCount = 5;

        private readonly SolidColorBrush _SnakeBodyBrush = Brushes.Green;
        private readonly SolidColorBrush _SnakeHeadBrush = Brushes.YellowGreen;
        private readonly List<SnakePart> _SnakeParts = [];

        public enum SnakeDirection { Left, Right, Up, Down };
        private SnakeDirection _SnakeDirection = SnakeDirection.Right;
        private int _SnakeLength;
        private int _CurrentScore = 0;

        private DispatcherTimer _GameTickTimer = new DispatcherTimer();

        private readonly Random _Round = new();

        private UIElement? _SnakeFood = null;
        private SolidColorBrush _FoodBrush = Brushes.Red;

        #endregion

        #region Property

        internal ObservableCollection<SnakeHighscore> HighscoreList
        {
            get; set;
        } = new ObservableCollection<SnakeHighscore>();

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
            _GameTickTimer.Tick += GameTickTimer_Tick;
            LoadHighscoreList();
        }

        #endregion

        #region Private methods

        #region UIElement Events
      
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SnakeDirection originalSnakeDirection = _SnakeDirection;
            switch (e.Key)
            {
                case Key.Up:
                    if (_SnakeDirection != SnakeDirection.Down)
                    {
                        _SnakeDirection = SnakeDirection.Up;
                    }
                    break;
                case Key.Down:
                    if (_SnakeDirection != SnakeDirection.Up)
                    {
                        _SnakeDirection = SnakeDirection.Down;
                    }
                    break;
                case Key.Left:
                    if (_SnakeDirection != SnakeDirection.Right)
                    {
                        _SnakeDirection = SnakeDirection.Left;
                    }
                    break;
                case Key.Right:
                    if (_SnakeDirection != SnakeDirection.Left)
                    {
                        _SnakeDirection = SnakeDirection.Right;
                    }
                    break;
                case Key.Space:
                    StartNewGame();
                    break;
            }
            if (_SnakeDirection != originalSnakeDirection)
            {
                MoveSnake();
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            DrawGameArea();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnShowHighscoreList_Click(object sender, RoutedEventArgs e)
        {
            this.bdrWelcomeMessage.Visibility = Visibility.Collapsed;
            this.bdrHighscoreList.Visibility = Visibility.Visible;
        }

        private void BtnAddToHighscoreList_Click(object sender, RoutedEventArgs e)
        {
            int newIndex = 0;
            // Where should the new entry be inserted?
            if ((this.HighscoreList.Count > 0) && (_CurrentScore < this.HighscoreList.Max(x => x.Score)))
            {
                SnakeHighscore justAbove = this.HighscoreList.OrderByDescending(x => x.Score).First(x => x.Score >= _CurrentScore);
                if (justAbove != null)
                {
                    newIndex = this.HighscoreList.IndexOf(justAbove) + 1;
                }
            }

            // Create & insert the new entry
            this.HighscoreList.Insert(newIndex, new SnakeHighscore()
            {
                PlayerName = txtPlayerName.Text,
                Score = _CurrentScore
            });

            // Make sure that the amount of entries does not exceed the maximum
            while (this.HighscoreList.Count > MaxHighscoreListEntryCount)
            {
                this.HighscoreList.RemoveAt(MaxHighscoreListEntryCount);
            }

            SaveHighscoreList();

            bdrNewHighscore.Visibility = Visibility.Collapsed;
            bdrHighscoreList.Visibility = Visibility.Visible;
        }

        #endregion

        #region Game
        
        private void DrawGameArea()
        {
            bool doneDrawingBackground = false;
            int nextX = 0, nextY = 0;
            int rowCounter = 0;
            bool nextIsOdd = false;

            while (!doneDrawingBackground)
            {
                Rectangle rect = new Rectangle
                {
                    Width = SnakeSquareSize,
                    Height = SnakeSquareSize,
                    Fill = nextIsOdd ? Brushes.White : Brushes.Black
                };
                GameArea.Children.Add(rect);
                Canvas.SetTop(rect, nextY);
                Canvas.SetLeft(rect, nextX);

                nextIsOdd = !nextIsOdd;
                nextX += SnakeSquareSize;
                int tempNextX = nextX + SnakeSquareSize;
                if (tempNextX > GameArea.ActualWidth)
                {
                    nextX = 0;
                    nextY += SnakeSquareSize;
                    rowCounter++;
                    nextIsOdd = (rowCounter % 2 != 0);
                }

                int tempNextY = nextY + SnakeSquareSize;
                if (tempNextY > GameArea.ActualHeight)
                {
                    doneDrawingBackground = true;
                }
            }
        }

        private void StartNewGame()
        {
            _CurrentScore = 0;
            _SnakeLength = SnakeStartLength;
            _SnakeDirection = SnakeDirection.Right;
            _SnakeParts.Clear();
            _SnakeParts.Add(new SnakePart() { Position = new Point(SnakeSquareSize * 5, SnakeSquareSize * 5) });
            _GameTickTimer.Interval = TimeSpan.FromMilliseconds(SnakeStartSpeed);
            this.bdrWelcomeMessage.Visibility = Visibility.Collapsed;
            this.bdrHighscoreList.Visibility = Visibility.Collapsed;
            this.bdrEndOfGame.Visibility = Visibility.Collapsed;

            //Draw new game area
            GameArea.Children.Clear();
            DrawGameArea();

            // Draw the snake  
            DrawSnake();
            DrawSnakeFood();

            // Update status
            UpdateGameStatus();

            // Go!          
            _GameTickTimer.IsEnabled = true;
        }

        private void EndGame()
        {
            bool isNewHighscore = false;
            if (_CurrentScore > 0)
            {
                int lowestHighscore = (this.HighscoreList.Count > 0 ? this.HighscoreList.Min(x => x.Score) : 0);
                if ((_CurrentScore > lowestHighscore) || (this.HighscoreList.Count < MaxHighscoreListEntryCount))
                {
                    this.bdrNewHighscore.Visibility = Visibility.Visible;
                    this.txtPlayerName.Focus();
                    isNewHighscore = true;
                }
            }
            if (!isNewHighscore)
            {
                this.tbFinalScore.Text = _CurrentScore.ToString();
                this.bdrEndOfGame.Visibility = Visibility.Visible;
            }
            _GameTickTimer.IsEnabled = false;
        }

        private void UpdateGameStatus()
        {
            this.tbStatusScore.Text = _CurrentScore.ToString();
            this.tbStatusSpeed.Text = _GameTickTimer.Interval.TotalMilliseconds.ToString();
        }

        private void GameTickTimer_Tick(object? sender, EventArgs e)
        {
            MoveSnake();
        }

        #endregion

        #region Snake
        private void DrawSnake()
        {
            foreach (SnakePart snakePart in _SnakeParts)
            {
                if (snakePart.UiElement == null)
                {
                    snakePart.UiElement = new Rectangle()
                    {
                        Width = SnakeSquareSize,
                        Height = SnakeSquareSize,
                        Fill = (snakePart.IsHead ? _SnakeHeadBrush : _SnakeBodyBrush)
                    };
                    GameArea.Children.Add(snakePart.UiElement);
                    Canvas.SetTop(snakePart.UiElement, snakePart.Position.Y);
                    Canvas.SetLeft(snakePart.UiElement, snakePart.Position.X);
                }
            }
        }

        private void MoveSnake()
        {
            // Remove the last part of the snake, in preparation of the new part added below  
            while (_SnakeParts.Count >= _SnakeLength)
            {
                GameArea.Children.Remove(_SnakeParts[0].UiElement);
                _SnakeParts.RemoveAt(0);
            }
            // Next up, we'll add a new element to the snake, which will be the (new) head  
            // Therefore, we mark all existing parts as non-head (body) elements and then  
            // we make sure that they use the body brush  
            foreach (SnakePart snakePart in _SnakeParts)
            {
                if (snakePart.UiElement is Rectangle snackePartRectangle)
                {
                    snackePartRectangle.Fill = _SnakeBodyBrush;
                    snakePart.IsHead = false;
                }
            }

            // Determine in which direction to expand the snake, based on the current direction  
            SnakePart snakeHead = _SnakeParts[_SnakeParts.Count - 1];
            double nextX = snakeHead.Position.X;
            double nextY = snakeHead.Position.Y;
            switch (_SnakeDirection)
            {
                case SnakeDirection.Left:
                    nextX -= SnakeSquareSize;
                    break;
                case SnakeDirection.Right:
                    nextX += SnakeSquareSize;
                    break;
                case SnakeDirection.Up:
                    nextY -= SnakeSquareSize;
                    break;
                case SnakeDirection.Down:
                    nextY += SnakeSquareSize;
                    break;
            }

            // Now add the new head part to our list of snake parts...  
            _SnakeParts.Add(new SnakePart()
            {
                Position = new Point(nextX, nextY),
                IsHead = true
            });
            //... and then have it drawn!  
            DrawSnake();
            // We'll get to this later...  
            DoCollisionCheck();
        }

        private void DoCollisionCheck()
        {
            SnakePart snakeHead = _SnakeParts[_SnakeParts.Count - 1];

            if ((snakeHead.Position.X == Canvas.GetLeft(_SnakeFood))
                && (snakeHead.Position.Y == Canvas.GetTop(_SnakeFood)))
            {
                EatSnakeFood();
                return;
            }

            if ((snakeHead.Position.Y < 0)
                || (snakeHead.Position.Y >= GameArea.ActualHeight)
                || (snakeHead.Position.X < 0)
                || (snakeHead.Position.X >= GameArea.ActualWidth))
            {
                EndGame();
            }

            foreach (SnakePart snakeBodyPart in _SnakeParts.Take(_SnakeParts.Count - 1))
            {
                if ((snakeHead.Position.X == snakeBodyPart.Position.X) && (snakeHead.Position.Y == snakeBodyPart.Position.Y))
                    EndGame();
            }
        }

        #endregion
      
        #region Food
        private void EatSnakeFood()
        {
            _SnakeLength++;
            _CurrentScore++;
            int timerInterval = Math.Max(SnakeSpeedThreshold, (int)_GameTickTimer.Interval.TotalMilliseconds - (_CurrentScore * 2));
            _GameTickTimer.Interval = TimeSpan.FromMilliseconds(timerInterval);
            GameArea.Children.Remove(_SnakeFood);
            DrawSnakeFood();
            UpdateGameStatus();
        }

        private Point GetNextFoodPosition()
        {
            int maxX = (int)(GameArea.ActualWidth / SnakeSquareSize);
            int maxY = (int)(GameArea.ActualHeight / SnakeSquareSize);
            int foodX = _Round.Next(0, maxX) * SnakeSquareSize;
            int foodY = _Round.Next(0, maxY) * SnakeSquareSize;

            foreach (SnakePart snakePart in _SnakeParts)
            {
                if ((snakePart.Position.X == foodX) && (snakePart.Position.Y == foodY))
                    return GetNextFoodPosition();
            }

            return new Point(foodX, foodY);
        }

        private void DrawSnakeFood()
        {
            Point foodPosition = GetNextFoodPosition();
            _SnakeFood = new Ellipse()
            {
                Width = SnakeSquareSize,
                Height = SnakeSquareSize,
                Fill = _FoodBrush
            };
            GameArea.Children.Add(_SnakeFood);
            Canvas.SetTop(_SnakeFood, foodPosition.Y);
            Canvas.SetLeft(_SnakeFood, foodPosition.X);
        }

        #endregion

        #region Score

        private void LoadHighscoreList()
        {
            if (!File.Exists("snake_highscorelist.xml"))
            {
                return;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<SnakeHighscore>));
            using Stream reader = new FileStream("snake_highscorelist.xml", FileMode.Open);
            var tempList = serializer.Deserialize(reader) as List<SnakeHighscore>;
            if (tempList is null)
            {
                return;
            }

            this.HighscoreList.Clear();
            foreach (var item in tempList!.OrderByDescending(x => x.Score))
            {
                this.HighscoreList.Add(item);
            }

        }

        private void SaveHighscoreList()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<SnakeHighscore>));
            using Stream writer = new FileStream("snake_highscorelist.xml", FileMode.Create);
            serializer.Serialize(writer, this.HighscoreList);
        }

        #endregion

        #endregion
  
    }
}