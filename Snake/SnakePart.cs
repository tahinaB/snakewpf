﻿using System.Windows;

namespace Snake
{
    internal class SnakePart(UIElement? pUiElement, Point pPosition, bool pIsHead)
    {
        public UIElement? UiElement
        {
            get => pUiElement;
            set
            {
                if (UIElement.Equals(pUiElement,value))
                {
                    return;
                }
                pUiElement = value;
            }
        }

        public Point Position
        {
            get => pPosition;
            set
            {
                if (Point.Equals(pPosition,value))
                {
                    return;
                }
                pPosition = value;
            }
        }

        public bool IsHead
        {
            get => pIsHead;
            set
            {
                if (pIsHead == value)
                {
                    return;
                }
                pIsHead = value;
            }
        }

        public SnakePart() : this(null, new Point(0, 0), false)
        {

        }
    }
}
